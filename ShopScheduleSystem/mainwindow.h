#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpServer>
#include <QLabel>
#include <iostream>
#include <QMessageBox>
#include <QStringList>
#include <QList>
#include <list>
#include "clientjobs.h"
#include "mythread.h"
#include "astar.h"
#include <sqlite.h>
#include <QSqlTableModel>
using namespace std;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    QLabel  *LabListen;//状态栏标签
    QLabel  *LabSocketState;//状态栏标签

    QTcpServer *tcpServer; //TCP服务器

    //QTcpSocket *tcpSocket;//TCP通讯的Socket

    //保存客户列表
    QStringList m_listIPAndPorts;  //IP_Port
    QList<ClientJobs*> m_listClients;//客户端
    QSqlTableModel *stm;
    //QString getLocalIP();//获取本机IP地址

public:
    explicit MainWindow(QWidget *parent = 0);
    int colmf,rowf,colml,rowl,num,colmn,rown,colms,rows,barrier=0,over=0;
    int colmf2,rowf2,colml2,rowl2,colmn2,rown2,colms2,rows2,barrier2=0,over2=0;
    ~MainWindow();

private slots:
    //自定义槽函数
    void    onNewConnection();//QTcpServer的newConnection()信号
    void    stregularpath(int colmf,int rowf,int barriern);
    void    onSocketStateChange(QAbstractSocket::SocketState socketState);
   // void    onClientConnected(); //Client Socket connected
    void    onClientDisconnected(QString strIPAndPort);//Client Socket disconnected
    void    onSocketRead(QByteArray temp,QString ipport,quint16 port,QString ip);//读取socket传入的数据

    void    StringToHex(QString str, QByteArray &senddata); //字符串转换为十六进制数据0-F
    char    ConvertHexChar(char ch);
    void    regularagv1(int colma,int rowa,int barriern);
    void    regularagv2(int colmb,int rowb,int barriern);
    void    sleep(unsigned int msec);
//程序生成的
    void on_newmap_clicked();

    void on_listenbutton_clicked();


    void on_staradrbtn_clicked();

    void on_finaladrbtn_clicked();

    void on_startcarbtn_clicked();

    void on_staradrbtn2_clicked();

    void on_finaladrbtn2_clicked();


    void on_startcarbtn2_clicked();

    void on_displayButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
