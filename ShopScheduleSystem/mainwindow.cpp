#include "mainwindow.h"
#include "ui_mainwindow.h"
#include  <QtNetwork>
#include <qstring.h>
#include <iostream>
#include "astar.h"
#include <qmessagebox.h>
#include <QTextStream>
#include <QApplication>
#include <QTime>
#include "record.h"
#include "sqlite.h"
using namespace std;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle("AGVRegularsystem");
    LabListen=new QLabel("监听状态:");
    LabListen->setMinimumWidth(150);
    ui->statusBar->addWidget(LabListen);

    LabSocketState=new QLabel("Socket状态：");//
    LabSocketState->setMinimumWidth(200);
    ui->statusBar->addWidget(LabSocketState);

    ui->tableWidget->setColumnCount(1);
    ui->tableWidget->setRowCount(1);

    ui->listenbutton->setEnabled(false);
    ui->finalcolm->setEnabled(false);
    ui->finalrowspin->setEnabled(false);
    ui->finaladrbtn->setEnabled(false);
    ui->startcarbtn->setEnabled(false);
    ui->staradrbtn->setEnabled(false);
    ui->finaladrbtn2->setEnabled(false);
    ui->startcarbtn2->setEnabled(false);
    ui->staradrbtn2->setEnabled(false);
    num=0;
    tcpServer=new QTcpServer(this);
    connect(tcpServer,SIGNAL(newConnection()),this,SLOT(onNewConnection()));
}
/*********************************************************************************************
 *析构器
 * *******************************************************************************************/
MainWindow::~MainWindow()       //析构器
{
    delete ui;
}
/*********************************************************************************************
 *字符串转换为十六进制数据0-F
 * *******************************************************************************************/
void MainWindow::StringToHex(QString str, QByteArray &senddata)
{
    int hexdata,lowhexdata;
    int hexdatalen = 0;
    int len = str.length();
    senddata.resize(len/2);
    char lstr,hstr;

    for(int i=0; i<len; )
    {
        //char lstr,
        hstr=str[i].toLatin1();
        if(hstr == ' ')
        {
            i++;
            continue;
        }
        i++;
        if(i >= len)
            break;
        lstr = str[i].toLatin1();
        hexdata = ConvertHexChar(hstr);
        lowhexdata = ConvertHexChar(lstr);
        if((hexdata == 16) || (lowhexdata == 16))
            break;
        else
            hexdata = hexdata*16+lowhexdata;
        i++;
        senddata[hexdatalen] = (char)hexdata;
        hexdatalen++;
    }
    senddata.resize(hexdatalen);
}
/*********************************************************************************************
 *把字母转换成对应十六进制数值
 * *******************************************************************************************/
char MainWindow::ConvertHexChar(char ch)
{
    if((ch >= '0') && (ch <= '9'))
        return ch-0x30;
    else if((ch >= 'A') && (ch <= 'F'))
        return ch-'A'+10;
    else if((ch >= 'a') && (ch <= 'f'))
        return ch-'a'+10;
    else return ch-ch;//不在0-f范围内的会发送成0
}
/*********************************************************************************************
 *接收Socket数据
 * *******************************************************************************************/
void MainWindow::onSocketRead(QByteArray temp,QString ipport,quint16 port,QString ip)
{
   sqlite s;
   s.connection();
   QByteArray senddata;
   QDataStream out(&temp,QIODevice::ReadWrite);    //将字节数组读入
   if(ipport==m_listIPAndPorts[0])
   {
     ui->plainTextEdit->insertPlainText("AGV1:");
     num=0;
   }
   else
   {
     ui->plainTextEdit->insertPlainText("AGV2:");
     num=1;
   }
   while(!out.atEnd())
   {
          qint8 outChar = 0;
          out>>outChar;   //每字节填充一次，直到结束
          //十六进制的转换
          QString str = QString("%1").arg(outChar&0xFF,2,16,QLatin1Char('0'));
          s.insert_one(ip,port,QString(str));
          ui->plainTextEdit->insertPlainText(str.toUpper());//大写
          ui->plainTextEdit->insertPlainText(" ");//每发送两个字符后添加一个空格
          ui->plainTextEdit->moveCursor(QTextCursor::End);
    }


   if((temp[0]==0x5a)&&(temp[1]==0x01))         //判断哪辆车接入
   {
     ui->staradrbtn->setEnabled(true);
   }
   else if((temp[0]==0x5a)&&(temp[1]==0x02))
   {
     ui->staradrbtn2->setEnabled(true);
   }


   if((temp[0]==0xdd)&&(temp[1]==0x01))             //得到AGV1起点
   {
      colmf=(temp[2]-1);
      rowf=(temp[3]-1);
      ui->startlabel->setText("起点:"+QString::number(temp[2])+","+QString::number(temp[3]));
      ui->finaladrbtn->setEnabled(true);
      ui->staradrbtn->setEnabled(false);
   }
   else if((temp[0]==0xdd)&&(temp[1]==0x02))        //得到AGV2起点
   {
       colmf2=(temp[2]-1);
       rowf2=(temp[3]-1);
       ui->startlabel2->setText("起点:"+QString::number(temp[2])+","+QString::number(temp[3]));
       ui->finaladrbtn2->setEnabled(true);
       ui->staradrbtn2->setEnabled(false);
   }


   if((temp[0]==0x5a)&&(temp[1]==0xa5))             //与AGV1交流
   {
     QTableWidgetItem *item = new QTableWidgetItem();
     item->setBackgroundColor(Qt::white);
     ui->tableWidget->setItem(colmf,rowf,item);
     if((temp[2]==colml+1)&&(temp[3]==rowl+1))
     {
         over=1;
         QTableWidgetItem *item2 = new QTableWidgetItem();
         item2->setBackgroundColor(Qt::red);
         ui->tableWidget->setItem(colml,rowl,item2);
         QString askaddr ="aa55ffff";//得到字符串
         StringToHex(askaddr,senddata);//将str字符串转换为16进制的形式
         m_listClients[num]->SendBytes(senddata);//发送到串口
     }
     if(over==0)
     {
      colmf=(temp[2]-1);
      rowf=(temp[3]-1);
      barrier=0;
      regularagv1(colmf,rowf,barrier);
      QColor color =ui->tableWidget->item(colmn,rown)->backgroundColor();
      QColor colgray=Qt::gray;
      QColor colgreen = Qt::green;
     if((color==colgray)||(color==colgreen))
     {
        barrier=1;
        cout<<barrier<<endl;
        regularagv1(colmf,rowf,barrier);
        barrier=0;
        QTableWidgetItem *item3 = new QTableWidgetItem();
        item3->setBackgroundColor(Qt::gray);
        ui->tableWidget->setItem(colmn2,rown2,item3);
        QTableWidgetItem *item4 = new QTableWidgetItem();
        item4->setBackgroundColor(Qt::green);
        ui->tableWidget->setItem(colmf2,rowf2,item4);
        QTableWidgetItem *item5 = new QTableWidgetItem();
        item5->setBackgroundColor(Qt::yellow);
        ui->tableWidget->setItem(colmn,rown,item5);
        QTableWidgetItem *item6 = new QTableWidgetItem();
        item6->setBackgroundColor(Qt::red);
        ui->tableWidget->setItem(colmf,rowf,item6);

        if((colmn!=colmn)&&(rown!=rowf))
        {
          QColor color1 =ui->tableWidget->item(colmn,rown-1)->backgroundColor();
          QColor color2 =ui->tableWidget->item(colmn+1,rown)->backgroundColor();
          QColor color3 =ui->tableWidget->item(colmn,rown-1)->backgroundColor();
          QColor color4 =ui->tableWidget->item(colmn-1,rown)->backgroundColor();
          if(((color1==colgray)||(color1==colgreen))&&((color2==colgray)||(color2==colgreen)))
          {
              QByteArray sendalarm;
              QString askaddr ="a1ff";//得到字符串
              StringToHex(askaddr,sendalarm);//将str字符串转换为16进制的形式
              m_listClients[num]->SendBytes(sendalarm);//发送到串口
              sleep(5);
          }
          else if(((color3==colgray)||(color3==colgreen))&&((color4==colgray)||(color4==colgreen)))
          {
              QByteArray sendalarm;
              QString askaddr ="a1ff";//得到字符串
              StringToHex(askaddr,sendalarm);//将str字符串转换为16进制的形式
              m_listClients[num]->SendBytes(sendalarm);//发送到串口
              sleep(5);
          }
        }

        QString askaddr ="aa11";//得到字符串
        StringToHex(askaddr,senddata);//将str字符串转换为16进制的形式
        senddata[2]=colmn; senddata[3]=rown;
        m_listClients[num]->SendBytes(senddata);//发送到串口
     }
     else
     {
         QTableWidgetItem *item7 = new QTableWidgetItem();
         item7->setBackgroundColor(Qt::gray);
         ui->tableWidget->setItem(colmn2,rown2,item7);
         QTableWidgetItem *item8 = new QTableWidgetItem();
         item8->setBackgroundColor(Qt::green);
         ui->tableWidget->setItem(colmf2,rowf2,item8);
         QTableWidgetItem *item9 = new QTableWidgetItem();
         item9->setBackgroundColor(Qt::yellow);
         ui->tableWidget->setItem(colmn,rown,item9);
         QTableWidgetItem *item10 = new QTableWidgetItem();
         item10->setBackgroundColor(Qt::red);
         ui->tableWidget->setItem(colmf,rowf,item10);

         if((colmn!=colmn)&&(rown!=rowf))
         {
           QColor color1 =ui->tableWidget->item(colmn,rown-1)->backgroundColor();
           QColor color2 =ui->tableWidget->item(colmn+1,rown)->backgroundColor();
           QColor color3 =ui->tableWidget->item(colmn,rown-1)->backgroundColor();
           QColor color4 =ui->tableWidget->item(colmn-1,rown)->backgroundColor();
           if(((color1==colgray)||(color1==colgreen))&&((color2==colgray)||(color2==colgreen)))
           {
               QByteArray sendalarm;
               QString askaddr ="a1ff";//得到字符串
               StringToHex(askaddr,sendalarm);//将str字符串转换为16进制的形式
               m_listClients[num]->SendBytes(sendalarm);//发送到串口
               sleep(5);
           }
           else if(((color3==colgray)||(color3==colgreen))&&((color4==colgray)||(color4==colgreen)))
           {
               QByteArray sendalarm;
               QString askaddr ="a1ff";//得到字符串
               StringToHex(askaddr,sendalarm);//将str字符串转换为16进制的形式
               m_listClients[num]->SendBytes(sendalarm);//发送到串口
               sleep(5);
           }
         }

         QString askaddr ="aa11";//得到字符串
         StringToHex(askaddr,senddata);//将str字符串转换为16进制的形式
         senddata[2]=colmn; senddata[3]=rown;
         m_listClients[num]->SendBytes(senddata);//发送到串口
     }
    }
   }


   if((temp[0]==0xa5)&&(temp[1]==0x5a))                 //与AGV2交流
   {
     QTableWidgetItem *item = new QTableWidgetItem();
     item->setBackgroundColor(Qt::white);
     ui->tableWidget->setItem(colmf2,rowf2,item);
     if((temp[2]==colml2+1)&&(temp[3]==rowl2+1))
     {
         over2=1;
         QTableWidgetItem *item2 = new QTableWidgetItem();
         item2->setBackgroundColor(Qt::red);
         ui->tableWidget->setItem(colml2,rowl2,item2);


         QString askaddr ="aa66ffff";//得到字符串
         StringToHex(askaddr,senddata);//将str字符串转换为16进制的形式
         m_listClients[num]->SendBytes(senddata);//发送到串口
     }
     if(over2==0)
     {
      colmf2=(temp[2]-1);
      rowf2=(temp[3]-1);
      barrier2=0;
      regularagv2(colmf2,rowf2,barrier2);
      QColor color =ui->tableWidget->item(colmn2,rown2)->backgroundColor();
      QColor colyellow=Qt::yellow;
      QColor colred=Qt::red;

     if((color==colyellow)||(color==colred))
     {
        barrier2=1;
        regularagv2(colmf2,rowf2,barrier2);
        barrier2=0;
        QTableWidgetItem *item3 = new QTableWidgetItem();
        item3->setBackgroundColor(Qt::gray);
        ui->tableWidget->setItem(colmn2,rown2,item3);
        QTableWidgetItem *item4 = new QTableWidgetItem();
        item4->setBackgroundColor(Qt::green);
        ui->tableWidget->setItem(colmf2,rowf2,item4);
        QTableWidgetItem *item5 = new QTableWidgetItem();
        item5->setBackgroundColor(Qt::yellow);
        ui->tableWidget->setItem(colmn,rown,item5);
        QTableWidgetItem *item6 = new QTableWidgetItem();
        item6->setBackgroundColor(Qt::red);
        ui->tableWidget->setItem(colmf,rowf,item6);

        if((colmn2!=colmn2)&&(rown2!=rowf2))
        {
          QColor color1 =ui->tableWidget->item(colmn2,rown2-1)->backgroundColor();
          QColor color2 =ui->tableWidget->item(colmn2+1,rown2)->backgroundColor();
          QColor color3 =ui->tableWidget->item(colmn2,rown2-1)->backgroundColor();
          QColor color4 =ui->tableWidget->item(colmn2-1,rown2)->backgroundColor();
          if(((color1==colyellow)||(color1==colred))&&((color2==colyellow)||(color2==colred)))
          {
              QByteArray sendalarm;
              QString askaddr ="a2ff";//得到字符串
              StringToHex(askaddr,sendalarm);//将str字符串转换为16进制的形式
              m_listClients[num]->SendBytes(sendalarm);//发送到串口
              sleep(5);
          }
          else if(((color3==colyellow)||(color3==colred))&&((color4==colyellow)||(color4==colred)))
          {
              QByteArray sendalarm;
              QString askaddr ="a2ff";//得到字符串
              StringToHex(askaddr,sendalarm);//将str字符串转换为16进制的形式
              m_listClients[num]->SendBytes(sendalarm);//发送到串口
              sleep(5);
          }
        }

        QString askaddr ="aa22";//得到字符串
        StringToHex(askaddr,senddata);//将str字符串转换为16进制的形式
        senddata[2]=colmn2; senddata[3]=rown2;
        m_listClients[num]->SendBytes(senddata);//发送到串口
     }
     else
     {
         QTableWidgetItem *item7 = new QTableWidgetItem();
         item7->setBackgroundColor(Qt::gray);
         ui->tableWidget->setItem(colmn2,rown2,item7);
         QTableWidgetItem *item8 = new QTableWidgetItem();
         item8->setBackgroundColor(Qt::green);
         ui->tableWidget->setItem(colmf2,rowf2,item8);
         QTableWidgetItem *item9 = new QTableWidgetItem();
         item9->setBackgroundColor(Qt::yellow);
         ui->tableWidget->setItem(colmn,rown,item9);
         QTableWidgetItem *item10 = new QTableWidgetItem();
         item10->setBackgroundColor(Qt::red);
         ui->tableWidget->setItem(colmf,rowf,item10);

         if((colmn2!=colmn2)&&(rown2!=rowf2))
         {
             QColor color1 =ui->tableWidget->item(colmn2,rown2-1)->backgroundColor();
             QColor color2 =ui->tableWidget->item(colmn2+1,rown2)->backgroundColor();
             QColor color3 =ui->tableWidget->item(colmn2,rown2-1)->backgroundColor();
             QColor color4 =ui->tableWidget->item(colmn2-1,rown2)->backgroundColor();
             if(((color1==colyellow)||(color1==colred))&&((color2==colyellow)||(color2==colred)))
             {
                 QByteArray sendalarm;
                 QString askaddr ="a2ff";//得到字符串
                 StringToHex(askaddr,sendalarm);//将str字符串转换为16进制的形式
                 m_listClients[num]->SendBytes(sendalarm);//发送到串口
                 sleep(5);
             }
             else if(((color3==colyellow)||(color3==colred))&&((color4==colyellow)||(color4==colred)))
             {
                 QByteArray sendalarm;
                 QString askaddr ="a2ff";//得到字符串
                 StringToHex(askaddr,sendalarm);//将str字符串转换为16进制的形式
                 m_listClients[num]->SendBytes(sendalarm);//发送到串口
             }
            }
         QString askaddr ="aa55";//得到字符串
         StringToHex(askaddr,senddata);//将str字符串转换为16进制的形式
         senddata[2]=colmn2; senddata[3]=rown2;
         m_listClients[num]->SendBytes(senddata);//发送到串口
         sleep(5);
     }
    }
     if(temp[0]==0xff)             //终点清除
     {
        if(temp[0]==0x01)
        {
           QTableWidgetItem *item11 = new QTableWidgetItem();
           item11->setBackgroundColor(Qt::white);
           ui->tableWidget->setItem(colmf,rowf,item11);
        }
        else if(temp[0]==0x02)
        {
            QTableWidgetItem *item11 = new QTableWidgetItem();
            item11->setBackgroundColor(Qt::white);
            ui->tableWidget->setItem(colmf2,rowf2,item11);
        }
     }
   }

}
/*********************************************************************************************
 *生成新的地图
 * *******************************************************************************************/
void MainWindow::on_newmap_clicked()
{
    int clom=ui->clomspin->value();
    int row=ui->rowspin->value();
    if(clom>0&&row>0)
    {
       // n=clom; m=row;
        ui->tableWidget->setColumnCount(clom);
        ui->tableWidget->setRowCount(row);
        ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
        //ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers); //设置表格内容不可编辑
        ui->tableWidget->setSelectionMode(QAbstractItemView::NoSelection); //设置表格内容不可选中
        ui->listenbutton->setEnabled(true);
    }
}
/*********************************************************************************************
 *监听控制
 * *******************************************************************************************/
void MainWindow::on_listenbutton_clicked()      //监听控制
{
    if(ui->listenbutton->text()=="开始监听")
    {
        ui->listenbutton->setText("停止监听");
        //开始监听
        QString     IP=ui->comboIP->currentText();//IP地址
        quint16     port=ui->portspin->value();//端口
        QHostAddress    addr(IP);
        tcpServer->listen(addr,port);
        LabListen->setText("监听状态：正在监听");
        ui->portspin->setEnabled(false);
        ui->comboIP->setEnabled(false);

    }
    else
    {  //停止监听
        ui->listenbutton->setText("开始监听");
        if (tcpServer->isListening()) //tcpServer正在监听
        {
            tcpServer->close();//停止监听
            ui->portspin->setEnabled(true);
            ui->comboIP->setEnabled(true);
            LabListen->setText("监听状态：已停止监听");
        }
      }
}
/*********************************************************************************************
 *新的客户端接入
 * *******************************************************************************************/
void MainWindow::onNewConnection()          //当有新的客户端接入服务器时
{

    QTcpSocket *tcpSocket = tcpServer->nextPendingConnection(); //创建socket
    QString strIPAndPort = tcpSocket->peerAddress().toString() + tr(" __ %1").arg(tcpSocket->peerPort());
    ui->plainTextEdit->appendPlainText("**client socket connected");
    ui->plainTextEdit->appendPlainText("**peer address:"+tcpSocket->peerAddress().toString());
    ui->plainTextEdit->appendPlainText("**peer port:"+QString::number(tcpSocket->peerPort()));
    connect(tcpSocket, SIGNAL(disconnected()),this, SLOT(onClientDisconnected(QString)));
    connect(tcpSocket,SIGNAL(stateChanged(QAbstractSocket::SocketState)),this,SLOT(onSocketStateChange(QAbstractSocket::SocketState)));
    onSocketStateChange(tcpSocket->state());
    //构建客户端对象
    ClientJobs *pClientJob = new ClientJobs(this, tcpSocket, strIPAndPort);
    //IP-port列表
    m_listIPAndPorts.append(strIPAndPort);
    //客户端列表
    m_listClients.append(pClientJob);
    connect(pClientJob,SIGNAL(readyRead()),this,SLOT(onSocketRead(QByteArray,QString,quint16,QString)));
    connect(pClientJob, SIGNAL(CallMainWindowDeleteClient(QString)), this, SLOT(onClientDisconnected(QString)));
    // 启动线程,传参sokect
    myThread *t = new myThread(tcpSocket);
    // 开始线程
    t->start();
    connect(t,SIGNAL(sendToMainWindow(QByteArray,QString,quint16,QString)),this,SLOT(onSocketRead(QByteArray,QString,quint16,QString)));

    ui->finalcolm->setEnabled(true);
    ui->finalrowspin->setEnabled(true);
}

void MainWindow::onSocketStateChange(QAbstractSocket::SocketState socketState)  //socket状态变化时
{
    switch(socketState)
    {
    case QAbstractSocket::UnconnectedState:
        LabSocketState->setText("scoket状态：UnconnectedState");
        break;
    case QAbstractSocket::HostLookupState:
        LabSocketState->setText("scoket状态：HostLookupState");
        break;
    case QAbstractSocket::ConnectingState:
        LabSocketState->setText("scoket状态：ConnectingState");
        break;

    case QAbstractSocket::ConnectedState:
        LabSocketState->setText("scoket状态：ConnectedState");
        break;

    case QAbstractSocket::BoundState:
        LabSocketState->setText("scoket状态：BoundState");
        break;

    case QAbstractSocket::ClosingState:
        LabSocketState->setText("scoket状态：ClosingState");
        break;

    case QAbstractSocket::ListeningState:
        LabSocketState->setText("scoket状态：ListeningState");
    }
}
/*********************************************************************************************
 *客户端断开连接
 * *******************************************************************************************/
void MainWindow::onClientDisconnected(QString strIPAndPort)         //客户端断开连接时
{
    ui->plainTextEdit->appendPlainText("**client socket disconnected");
    int nIndex = m_listIPAndPorts.indexOf(strIPAndPort);
    if(nIndex < 0)
    {
        //序号出错
        return;
    }
    //删除对应客户端
    m_listIPAndPorts.removeAt(nIndex); //删除IP字符串列表对应条目
    m_listClients.removeAt(nIndex);//删除客户端

}

/*********************************************************************************************
 *获取AGV1发车点
 * *******************************************************************************************/
void MainWindow::on_staradrbtn_clicked()        //获取AGV1发车点
{
    QString askaddr ="af01" ;//得到字符串
    QByteArray senddata;

     StringToHex(askaddr,senddata);//将str字符串转换为16进制的形式

     m_listClients[0]->SendBytes(senddata);//发送到串口
}
/*********************************************************************************************
 *设置AGV1任务终点
 * *******************************************************************************************/
void MainWindow::on_finaladrbtn_clicked()       //设置AGV1任务终点
{
    colml=(ui->finalcolm ->value())-1;
    rowl=(ui->finalrowspin->value())-1;
    ui->finaladrbtn->setEnabled(false);
    ui->startcarbtn->setEnabled(true);
}
/*********************************************************************************************
 *AGV1发车路径规划
 * *******************************************************************************************/
void MainWindow::on_startcarbtn_clicked()       //发车路径规划
{
    int count=0;
    int col=ui->clomspin->value();
    int row=ui->rowspin->value();
    //0表示空地  1表示障碍
    vector<std::vector<int> > regu(row,vector<int>(col,0));
    Astar astar;
    astar.InitAstar(regu);
    Point start(colmf,rowf);
    Point end(colml,rowl);
    list<Point*> path = astar.GetPath(start,end,false);
    for(auto &p:path)
     {
        QTableWidgetItem *item = new QTableWidgetItem();
//        item->setBackgroundColor(Qt::blue);
        ui->tableWidget->setItem(p->x,p->y,item);
        if(count==1)
        {
            QString askaddr ="ac01";//得到字符串
            QByteArray senddata;
             StringToHex(askaddr,senddata);//将str字符串转换为16进制的形式
             senddata[2]=(p->x)+1;senddata[3]=(p->y)+1;
             colmn=(p->x); rown=(p->y);
             QTableWidgetItem *item = new QTableWidgetItem();
             item->setBackgroundColor(Qt::yellow);
             ui->tableWidget->setItem(p->x,p->y,item);
             m_listClients[0]->SendBytes(senddata);//发送到串口
        }
        count++;
        cout<<'('<<p->x<<','<<p->y<<')'<<endl;
     }
    QTableWidgetItem *item = new QTableWidgetItem();
    item->setBackgroundColor(Qt::red);
    ui->tableWidget->setItem(colmf,rowf,item);
    ui->finalcolm->setEnabled(false);
    ui->finalrowspin->setEnabled(false);
}
/*********************************************************************************************
 *AGV2发车与AGV1冲突路径规划
 * *******************************************************************************************/
void MainWindow::stregularpath(int colmf,int rowf,int barriern)
{
    int count=0;
    int col=ui->clomspin->value();
    int row=ui->rowspin->value();
    //0表示空地  1表示障碍
    vector<std::vector<int> > regu(row,vector<int>(col,0));
    if(barriern==1)
    {
        regu[colmn][rown]=1;
        regu[colmf][rowf]=1;//
        //cout<<colmn<<endl;
        //cout<<rown<<endl;
        //regu[0][1]=1;
    }
    Astar astar;
    astar.InitAstar(regu);
    Point start(colmf,rowf);
    Point end(colml2,rowl2);
    list<Point*> path = astar.GetPath(start,end,false);
    for(auto &p:path)
     {
        QTableWidgetItem *item = new QTableWidgetItem();
//        item->setBackgroundColor(Qt::blue);
        ui->tableWidget->setItem(p->x,p->y,item);
        if(count==1)
        {
             colmn2=(p->x); rown2=(p->y);
        }
        count++;
        cout<<'('<<p->x<<','<<p->y<<')'<<endl;
     }
}

/*********************************************************************************************
 *获取AGV2发车点
 * *******************************************************************************************/
void MainWindow::on_staradrbtn2_clicked()           //获取AGV2发车点
{
    QString askaddr ="af02" ;//得到字符串
    QByteArray senddata;

     StringToHex(askaddr,senddata);//将str字符串转换为16进制的形式

     m_listClients[1]->SendBytes(senddata);//发送到串口
}

void MainWindow::on_finaladrbtn2_clicked()          //设置AGV2终点
{
    colml2=(ui->finalcolm2 ->value())-1;
    rowl2=(ui->finalrowspin2->value())-1;
    ui->finaladrbtn2->setEnabled(false);
    ui->startcarbtn2->setEnabled(true);
}
/*********************************************************************************************
 *AGV2发车
 * *******************************************************************************************/
void MainWindow::on_startcarbtn2_clicked()
{
    int count=0;
    int col=ui->clomspin->value();
    int row=ui->rowspin->value();
    //0表示空地  1表示障碍
    vector<std::vector<int> > regu(row,vector<int>(col,0));
    //regu[0][1]=1;
    Astar astar;
    astar.InitAstar(regu);
    Point start(colmf2,rowf2);
    Point end(colml2,rowl2);
    list<Point*> path = astar.GetPath(start,end,false);
    for(auto &p:path)
     {
        QTableWidgetItem *item1 = new QTableWidgetItem();
//        item1->setBackgroundColor(Qt::blue);
        ui->tableWidget->setItem(p->x,p->y,item1);
        if(count==1)
        {
            QString askaddr ="ac02";//得到字符串
            QByteArray senddata;
             StringToHex(askaddr,senddata);//将str字符串转换为16进制的形式
             senddata[2]=(p->x)+1;senddata[3]=(p->y)+1;
             colmn2=(p->x); rown2=(p->y);
             m_listClients[1]->SendBytes(senddata);//发送到串口
        }
        count++;
        cout<<'('<<p->x<<','<<p->y<<')'<<endl;
     }
   QTableWidgetItem *item2 = new QTableWidgetItem();
   item2->setBackgroundColor(Qt::red);
   ui->tableWidget->setItem(colmf,rowf,item2);
   QTableWidgetItem *item8 = new QTableWidgetItem();
   item8->setBackgroundColor(Qt::yellow);
   ui->tableWidget->setItem(colmn,rown,item8);
   QTableWidgetItem *item3 = new QTableWidgetItem();
   if((colmn==colmn2)&&(rown==rown2))
   {
       barrier2=1;
       stregularpath(colmf2,rowf2,barrier2);
       barrier2=0;
       item3->setBackgroundColor(Qt::gray);
       ui->tableWidget->setItem(colmn2,rown2,item3);
       QTableWidgetItem *item4 = new QTableWidgetItem();
       item4->setBackgroundColor(Qt::green);
       ui->tableWidget->setItem(colmf2,rowf2,item4);
       QTableWidgetItem *item5 = new QTableWidgetItem();
       item5->setBackgroundColor(Qt::red);
       ui->tableWidget->setItem(colmf,rowf,item5);
       QTableWidgetItem *item6 = new QTableWidgetItem();
       item6->setBackgroundColor(Qt::yellow);
       ui->tableWidget->setItem(colmn,rown,item6);

       QString askaddr ="ac02";//得到字符串
       QByteArray senddata;
       StringToHex(askaddr,senddata);//将str字符串转换为16进制的形式
       senddata[2]=colmn2; senddata[3]=rown2;
       m_listClients[num]->SendBytes(senddata);//发送到串口

   }
   else
   {
    item3->setBackgroundColor(Qt::gray);
    ui->tableWidget->setItem(colmn2,rown2,item3);
    QTableWidgetItem *item7 = new QTableWidgetItem();
    item7->setBackgroundColor(Qt::green);
    ui->tableWidget->setItem(colmf2,rowf2,item7);
   }
    ui->finalcolm2->setEnabled(false);
    ui->finalrowspin2->setEnabled(false);
}
/*********************************************************************************************
 *AGV1行驶任务调度以及冲突再规划
 * colma:当前位置colm
 * rowa：当前位置row
 * barriern：遇到冲突标识符
 * *******************************************************************************************/

void MainWindow::regularagv1(int colma,int rowa,int barriern)
{
    int count=0;
    int col= ui->clomspin->value();
    int row=ui->rowspin->value();
    //0表示空地  1表示障碍
    vector<std::vector<int> > regu(row,vector<int>(col,0));
    if(barriern==1)
    {
        regu[colmn2][rown2]=1;
        regu[colmf2][rowf2]=1;//
        regu[colms2][rows2]=1;
        //regu[0][1]=1;
    }
    Astar astar;
    astar.InitAstar(regu);
    Point start(colma,rowa);
    Point end(colml,rowl);
    list<Point*> path = astar.GetPath(start,end,false);
    for(auto &p:path)
     {
        QTableWidgetItem *item = new QTableWidgetItem();
//        item->setBackgroundColor(Qt::blue);
        ui->tableWidget->setItem(p->x,p->y,item);
        if(count==1)
        {
             colmn=(p->x); rown=(p->y);
        }
        if(count==2)//半阻塞
        {
             colms=(p->x); rows=(p->y);
        }
        count++;
        cout<<'('<<p->x<<','<<p->y<<')'<<endl;
     }
    QTableWidgetItem *item2 = new QTableWidgetItem();
    item2->setBackgroundColor(Qt::gray);
    ui->tableWidget->setItem(colmn2,rown2,item2);
    QTableWidgetItem *item3 = new QTableWidgetItem();
    item3->setBackgroundColor(Qt::green);
    ui->tableWidget->setItem(colmf2,rowf2,item3);
}
/*********************************************************************************************
 *AGV2行驶任务调度以及冲突再规划
 * colmb:当前位置colm
 * rowb：当前位置row
 * barriern：遇到冲突标识符
 * *******************************************************************************************/
void MainWindow::regularagv2(int colmb,int rowb,int barriern)
{
    int count=0;
    int col=ui->clomspin->value();
    int row=ui->rowspin->value();
    //0表示空地  1表示障碍
    vector<std::vector<int> > regu(row,vector<int>(col,0));
    if(barriern==1)
    {
        regu[colmn][rown]=1;
        regu[colmf][rowf]=1;//
        regu[colms][rows]=1;
        //regu[0][1]=1;
    }
    Astar astar;
    astar.InitAstar(regu);
    Point start(colmb,rowb);
    Point end(colml2,rowl2);
    list<Point*> path = astar.GetPath(start,end,false);
    for(auto &p:path)
     {
        QTableWidgetItem *item = new QTableWidgetItem();
//        item->setBackgroundColor(Qt::blue);
        ui->tableWidget->setItem(p->x,p->y,item);
        if(count==1)
        {
             colmn2=(p->x); rown2=(p->y);
        }
        if(count==2)//半阻塞
        {
             colms2=(p->x); rows2=(p->y);
        }
        count++;
        cout<<'('<<p->x<<','<<p->y<<')'<<endl;
     }
    QTableWidgetItem *item2 = new QTableWidgetItem();
    item2->setBackgroundColor(Qt::yellow);
    ui->tableWidget->setItem(colmn,rown,item2);
    QTableWidgetItem *item3 = new QTableWidgetItem();
    item3->setBackgroundColor(Qt::red);
    ui->tableWidget->setItem(colmf,rowf,item3);
}
/*********************************************************************************************
 *毫秒级别系统延时函数
 * msec:延时时间ms
 * *******************************************************************************************/
void MainWindow::sleep(unsigned int msec)
{
    QTime dieTime = QTime::currentTime().addMSecs(msec);
    while( QTime::currentTime() < dieTime )
        QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}
/***********************************************************************************************
 * 可视化数据库
 * ********************************************************************************************/
void MainWindow::on_displayButton_clicked()
{
    //this->close();
    sqlite s;
    s.connection();
    stm = new QSqlTableModel; //初始化对象
    stm->setTable("information");
    stm->select(); // 显示数据
    record *c = new record(stm);
    c->show();
}
