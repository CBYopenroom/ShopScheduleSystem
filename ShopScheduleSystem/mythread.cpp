#include "mythread.h"
#include <qhostaddress.h>

myThread::myThread(QTcpSocket *s)
{
    // 传参来的socket赋值给private中的socket
    socket = s;
}
void myThread::run()
{
    connect(socket,SIGNAL(readyRead()),this,SLOT(clientInfoSlot()));
}

void myThread::clientInfoSlot()
{
    // 不能在其他类里面操作界面
    // qDebug() << socket->readAll(); // 可通过自定义信号实现操作ui
    QByteArray temp;
    QString ipport = socket->peerAddress().toString()+tr(" __ %1").arg(socket->peerPort());
    quint16 port = socket->peerPort();
    QString ip = socket->peerAddress().toString();
    temp.append(socket->readAll());
    emit sendToMainWindow(temp,ipport,port,ip); //emit: 发送自定义信号
}
